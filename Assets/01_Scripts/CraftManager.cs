using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftManager : MonoBehaviour
{
    public Receipe[] receipes;
    public Ingredient[] ingredients;

    public GameObject itemPrefab;

    private void Awake()
    {
        receipes = Resources.LoadAll<Receipe>("Receipes");
        ingredients = Resources.LoadAll<Ingredient>("Ingredients");
    }

    public void CraftItem(Ingredient itemToCraft, RectTransform location)
    {
        if (itemPrefab)
        {
            GameObject result = Instantiate(itemPrefab, location.position, Quaternion.identity, gameObject.transform);

            Item resultItem = result.GetComponent<Item>();

            resultItem.itemInfos = itemToCraft;

            resultItem.UpdateFromScriptable();

            Debug.Log("tentative de cr�er l'item");
        }
    }

    public void ClearReceipeStates()
    {
        foreach(Receipe receipe in receipes)
        {
            receipe.ClearStates();
        }
    }
}
