using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IDragHandler, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDropHandler
{
    public Ingredient itemInfos;
    private CanvasGroup canvasGroup;
    private RectTransform rectTransform;
    private CraftManager craftManager;
    
    [SerializeField] Ingredient itemToCraft;

    private int id = 0;
    private string _name = "";

    public int ID
    {
        get { return id; }
    }

    public string Name
    {
        get { return _name; }
    }

    private void Awake()
    {
        UpdateFromScriptable();
        canvasGroup = GetComponent<CanvasGroup>();
        rectTransform = GetComponent<RectTransform>();
        craftManager = FindObjectOfType<CraftManager>();
    }

    public void UpdateFromScriptable()
    {
        GetComponent<Image>().sprite = itemInfos.Illustration;
        id = itemInfos.ID;
        _name = itemInfos.Name;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("Drag");

        rectTransform.position += new Vector3(eventData.delta.x, eventData.delta.y, 0f);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("BeginDrag");

        canvasGroup.blocksRaycasts = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("EndDrag");

        canvasGroup.blocksRaycasts = true;
        craftManager.ClearReceipeStates();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("PointerDown");
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Drop");

        Item dragedItem = eventData.pointerDrag.GetComponent<Item>();

        if (dragedItem.GetInstanceID() != GetInstanceID())
        {
            CheckCraft(dragedItem);
        }

        if (itemToCraft)
        {
            craftManager.CraftItem(itemToCraft, rectTransform);
            Destroy(dragedItem.gameObject);
            Destroy(this.gameObject);
        }
    }

    private void CheckCraft(Item dragedItem)
    {
        itemToCraft = null;
        craftManager.ClearReceipeStates();

        foreach (Receipe receipe in craftManager.receipes)
        {
            foreach(Ingredient ingredient in receipe.ingredients)
            {
                if (dragedItem.ID == ingredient.ID || ID == ingredient.ID)
                {
                    receipe.receipeState.Add(true);
                }
            }

            if (receipe.receipeState.Count == receipe.ingredients.Count)
            {
                itemToCraft = receipe.result;
                craftManager.ClearReceipeStates();
                return;
            }              
        }
    }
}
