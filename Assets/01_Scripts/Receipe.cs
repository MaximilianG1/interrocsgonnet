using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Receipe", order = 2)]
public class Receipe : ScriptableObject
{
    public List<Ingredient> ingredients;
    public List<bool> receipeState;
    public Ingredient result;

    public void ClearStates()
    {
        receipeState.Clear();
    }

}
